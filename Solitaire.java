package solitaire;

import java.util.*;
import java.awt.Point;

/*
 * a nice game of solitaire
 */
public class Solitaire {
    private LinkedCardList[] foundation; // foundation for each suit, 1st column is
                                         // clubs, 2nd is diamonds, 3rd is hearts, 4th is
                                         // spades
    private PackOfCards pack;
    private LinkedCardList[] tableau; // tableau of 7 cards
    private HashMap<String, Point> locations; // x is column number, y is row number
    private final String CLUBS = "Clubs";
    private final String DIAMONDS = "Diamonds";
    private final String HEARTS = "Hearts";
    private final String SPADES = "Spades";
    private LinkedCardList hand;

    public Solitaire() {
        pack = new PackOfCards();
        pack.shuffle();
        locations = new HashMap<>();
        hand = new LinkedCardList();
        createFoundation();
        createTableau(pack);
    }

    // pre: all cards in the foundation are in ascending order
    public boolean winCondition() { // checks if you won the game

        for (int a = 0; a < 4; a++) {
            if (tableau[a].getLength() < 13) {
                return false;
            }
        }
        return true;
    }

    public void createFoundation() {
        foundation = new LinkedCardList[4];
        for (int i = 0; i < 4; i++) {
            foundation[i] = new LinkedCardList();
        }
    }

    public void createTableau(PackOfCards pack) {
        tableau = new LinkedCardList[7];

        for (int a = 0; a < tableau.length; a++) {
            tableau[a] = new LinkedCardList();
            for (int b = 0; b <= a; b++) {
                Card card = pack.getLastCard();
                String cardName = card.toString();
                if (b == a) {
                    tableau[a].add(card);
                } else {
                    card.setState(false);
                    tableau[a].add(card);
                }
                locations.put(cardName.toLowerCase(), new Point(a, b));
            }
        }
    }

    public String printTableau() {
        StringBuilder cards = new StringBuilder();
        for (int a = 0; a < tableau.length; a++) {

            cards.append(tableau[a].toString() + "\n");
        }
        return cards.toString();
    }

    public String printHand() {
        return hand.toString();
    }

    public String printFoundation() {
        StringBuilder cards = new StringBuilder();
        for (int i = 0; i < foundation.length; i++) {
            cards.append(foundation[i].toString() + "\n");
        }
        return cards.toString();
    }

    // pre: attachCol will be 0-based indexing
    // post: returns whether or not moving a group to a column is allowed
    public boolean canMoveTableau(String name, int attachCol) { // check if moving a
                                                                // certain group of
                                                                // cards to another
                                                                // group (all in tableau) is allowed

        if (outOfBounds(attachCol)) {
            return false;
        }
        Point detachPoint = locations.get(name.toLowerCase());
        if (detachPoint == null) {
            return false;
        }

        if (!tableau[(int) detachPoint.getX()].getCard((int) detachPoint.getY()).getState()) { // if the card is flipped
                                                                                               // over (think of
                                                                                               // something like X, 10,
                                                                                               // 9, where X is the
                                                                                               // flipped over card),
                                                                                               // return false
            return false;
        }

        // if user wants to move group into an empty column, always return true
        if (tableau[attachCol].getLength() == 0) {
            return true;
        }

        Card tail = tableau[attachCol].getLastCard();
        Card head = tableau[(int) (detachPoint.getX())].getCard((int) detachPoint.getY());

        // reds must be attached to blacks in descending order (and vice versa) AND tail
        // must be 1 higher than head
        return descendingOrder(tail, head) && alternatingColors(tail, head);

    }

    // pre: will always check whether CARD can be put in foundation, not a GROUP of
    // cards (in other words, the last card in the column of cards specified)
    // pre: col is based on 0-based indexing
    // post: returns whether or not CARD can be placed in foundation at this moment
    // (the last card)
    public boolean canMoveFoundation(int col) {
        if (outOfBounds(col)) {
            return false;
        }
        Card card = tableau[col].getLastCard();
        String suit = card.getSuit();
        int rank = card.getRankNum();
        int foundationindex = 0;
        switch (suit) {
            case CLUBS:
                foundationindex = 0;
                break;
            case DIAMONDS:
                foundationindex = 1;
                break;
            case HEARTS:
                foundationindex = 2;
                break;
            case SPADES:
                foundationindex = 3;
                break;
            default:
                foundationindex = -1;
                break;
        }
        return (rank == 1 && foundation[foundationindex].getLength() == 0)
                || (foundation[foundationindex].getLength() > 0
                        && foundation[foundationindex].getLastCard().getRankNum() == rank - 1);
    }

    // pre: col is based on 0-based indexing
    // post: returns whether or not moving the last card from the hand to the column
    // specified is valid
    public boolean canMoveHand(int col) {
        if (hand.getLength() == 0) {
            return false;
        }
        if (tableau[col].getLength() == 0) {
            return true;
        }
        if (outOfBounds(col)) {
            return false;
        }
        Card tail = tableau[col].getLastCard();
        Card head = hand.getLastCard();
        return descendingOrder(tail, head) && alternatingColors(tail, head);
    }

    public boolean canAddToHand() {
        return pack.getNumCards() > 0;
    }

    public boolean descendingOrder(Card first, Card second) { // first card will be above second card in a list
        return first.getRankNum() == second.getRankNum() + 1;
    }

    // pre: col is based on 0-based indexing
    // post: returns true if col is out of range of 0-6 inclusive, false if in range
    // of 0-6 inclusive
    public boolean outOfBounds(int col) {
        return (col > tableau.length - 1) || col < 0;
    }

    public boolean alternatingColors(Card first, Card second) {
        String firstSuit = first.getSuit();
        String secondSuit = second.getSuit();
        switch (firstSuit) {
            case CLUBS:
                if (secondSuit.equals(SPADES)) {
                    return false;
                }
                break;
            case DIAMONDS:
                if (secondSuit.equals(HEARTS)) {
                    return false;
                }
                break;
            case HEARTS:
                if (secondSuit.equals(DIAMONDS)) {
                    return false;
                }
                break;
            case SPADES:
                if (secondSuit.equals(CLUBS)) {
                    return false;
                }
                break;
            default:
                System.out.println("How did you get a card thats not part of the 4 suits");
                return false;
        }
        return true;
    }

    // pre: attachIndex will be based on 0-based indexing
    // post: moves a group of cards at a column location specified and returns the
    // previous state
    public SolitaireState moveTableau(String detachLocation, int attachIndex) {
        SolitaireState saveState = this.save(); // save the previous state before modifying any changes
        Point detachPoint = locations.get(detachLocation);
        LinkedCardList detachedList = tableau[(int) detachPoint.getX()].remove((int) detachPoint.getY());
        int startLocation = tableau[attachIndex].getLength();
        for (int i = 0; i < detachedList.getLength(); i++) {
            locations.replace(detachedList.getCard(i).toString().toLowerCase(),
                    new Point(attachIndex, startLocation + i));
        }
        tableau[attachIndex].add(detachedList);
        if (tableau[(int) detachPoint.getX()].getLength() > 0) {
            tableau[(int) detachPoint.getX()].getLastCard().setState(true);
        }
        return saveState;
    }

    // pre: card will always be a list of size 1, col will be based on 0-based
    // indexing
    // post: moves last card in hand to column specified and returns the previous
    // state
    public SolitaireState moveHand(int col) {
        SolitaireState saveState = this.save(); // save the previous state before modifying any changes
        Card card = hand.removeLastCard();
        locations.put(card.toString().toLowerCase(), new Point(col, tableau[col].getLength()));
        tableau[col].add(card);
        return saveState;
    }

    // pre: col is based on 0 based indexing
    // post: moves last card in column specified into foundation and returns the
    // previous state
    public SolitaireState moveFoundation(int col) {
        SolitaireState saveState = this.save(); // save the previous state before modifying any changes
        Card card = tableau[col].removeLastCard();
        String suit = card.getSuit();
        int foundationindex = 0;
        switch (suit) {
            case CLUBS:
                foundationindex = 0;
                break;
            case DIAMONDS:
                foundationindex = 1;
                break;
            case HEARTS:
                foundationindex = 2;
                break;
            case SPADES:
                foundationindex = 3;
                break;
            default:
                foundationindex = -1;
                break;
        }
        locations.replace(card.getSuit().toLowerCase(),
                new Point(foundationindex * -1, foundation[foundationindex].getLength()));
        foundation[foundationindex].add(card);
        if (tableau[col].getLength() > 0) {
            tableau[col].getLastCard().setState(true);
        }
        return saveState;
    }

    // adds a card to the hand; if there are more than 3 cards, the first card is
    // removed, and also returns the previous state
    public SolitaireState addToHand() {
        SolitaireState saveState = this.save(); // save the previous state before modifying any changes
        hand.add(pack.getLastCard());

        if (hand.getLength() > 3) {
            Card head = hand.getFirstCard();
            hand = hand.remove(1);
            pack.addToFront(head);
        }
        return saveState;
    }

    public LinkedCardList[] getTableau() {
        return this.tableau;
    }

    public LinkedCardList[] getFoundation() {
        return this.foundation;
    }

    public LinkedCardList getHand() {
        return hand;
    }

    public Map<String, Point> getLocations() {
        return locations;
    }

    public boolean containKey(String key) {
        return locations.containsKey(key);
    }

    public SolitaireState save() {
        return new SolitaireState(foundation, tableau, pack, locations, hand);
    }

    public void restore(SolitaireState saveState) {
        tableau = saveState.getTableau();
        foundation = saveState.getFoundation();
        pack = saveState.getPack();
        locations = (HashMap<String, Point>) saveState.getLocations();
        hand = saveState.getHand();
    }
}
