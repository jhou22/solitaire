package solitaire;
import java.util.*;
import java.awt.*;

// look up memento state programming for further explanation
public class SolitaireState {
    private final LinkedCardList[] foundation;
    private final LinkedCardList[] tableau;
    private final PackOfCards pack;
    private final HashMap<String, Point> locations;
    private final LinkedCardList hand;

    public SolitaireState(LinkedCardList[] foundation, LinkedCardList[] tableau, PackOfCards pack, Map<String, Point> locations, LinkedCardList hand) {
        this.foundation = foundation;
        this.tableau = tableau;
        this.pack = pack;
        this.locations = (HashMap<String, Point>) locations;
        this.hand = hand;
    }

    public LinkedCardList[] getFoundation() {
        return foundation;
    }

    public LinkedCardList[] getTableau() {
        return tableau;
    }

    public PackOfCards getPack() {
        return pack;
    }

    public Map<String, Point> getLocations() {
        return locations;
    }

    public LinkedCardList getHand() {
        return hand;
    }
}
