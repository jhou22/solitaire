package solitaire;
import java.util.*;

/*
 * A pack of cards that can show its front AND back
 */
public class PackOfCards {
    private ArrayList<Card> deck;
    public PackOfCards() { // initialized as a brand new, unopened pack of cards, with the front shown
        deck = new ArrayList<>();
        String[] suits = {"Spades", "Diamonds", "Clubs", "Hearts"};
        for (int a = 0; a < 52; a++) {
            deck.add(new Card(((a % 13) + 1), suits[a / 13], true));
        }
    }

    public void shuffle() { // runs the fisher-yates shuffle algorithm to shuffle deck of cards
        Random randInt = new Random();

        for (int a = deck.size() - 1; a > 0; a--) {

            int b = randInt.nextInt(a + 1);

            Card temp = deck.get(a);
            deck.set(a, deck.get(b));
            deck.set(b, temp);
        }
    }

    public String toString() { // returns a string representation of a card
        String deckOfCards = "";
        for (int a = 0; a < deck.size(); a++) {
            deckOfCards += "(" + deck.get(a).toString() + "), ";
        }
        return deckOfCards;
    }

    public Card getCard(int index) { // returns a card at a specific index;
        return deck.get(index);
    }
    
    public Card getLastCardReplacement() { // returns a card starting from the back with replacement
        return deck.get(deck.size() - 1);
    }

    public int getNumCards() {
        return deck.size();
    }

    public Card getLastCard() { // returns a card starting from the back without replacement
        Card card = deck.get(deck.size() - 1);
        deck.remove(deck.size() - 1);
        return card;
    }
    public void setCardState(int index, boolean state) {
        deck.get(index).setState(state);
    }
    public void addToFront(Card card) {
        deck.add(0, card);
    }
}
