package solitaire;

/*
 * LinkedList of cards for klondike solitaire
 */
public class LinkedCardList {
    Node head;
    Node tail;
    int length;

    class Node { // inner Node class
        Node next;
        Card card;

        public Node(Card card) {
            this.card = card;
            next = null;
            
        }

    }

    public LinkedCardList(Card card) { // constructing a list from a card
        head = new Node(card);
        tail = head;
        length = 1;
    }

    public LinkedCardList(Node n) { // constructing a list from a node
        head = n;
        tail = head;
        length = 0;
        Node trav = n;
        while (trav.next != null) {
            length++;
            trav = trav.next;
        }
        tail = trav;
        length++;
        
    }

    public LinkedCardList() { // constructing an empty list
        head = null;
        tail = null;
        length = 0;
    }

    /*
     * getter methods
     */
    public Node getHead() {
        return head;
    }

    public Node getTailNext() {
        return tail.next;
    }

    public Node getTail() {
        return tail;
    }

    public Card getLastCard() {
        return tail.card;
    }

    public Card getFirstCard() {
        return head.card;
    }

    public int getLength() {
        return length;
    }

    public Card getCard(int index) { // gets a card at a specific index (0-index based)
        int count = 0;
        Node trav = head;
        while (count < index) {
            trav = trav.next;
            count++;
        }
        return trav.card;
    }

    public int getIndex(Card c) { // gets the location of the card (1-index based)
        int count = 0;
        Node trav = head;
        while (trav != null && trav.card.toString().equals(c.toString())) {
            count++;
            trav = trav.next;
        }
        return count;
    }
    
    /*
     * setter methods
     */

    public void add(Card c) { // adds a singular card
        Node card = new Node(c);
        if (head == null) {
            head = card;
            tail = head;
            length++;
        } else {
            tail.next = card;
            tail = card;
            length++;
        }

    }

    public void add(LinkedCardList l) { // add a group of cards to the end of an already created group
        if (head == null) {
            head = l.head;
            tail = l.tail;
            this.length = l.length;
        } else {
            tail.next = l.head;
            tail = l.tail;
            this.length += l.length;
        }
    }
    
    public Card removeLastCard() {
        if (length < 1) {
            return null;
        }
        if (length == 1) {
            return head.card;
        }

        Node trav = head;
        int count = 0;
        while (trav.next.next != null) {
            trav = trav.next;
            
        }
        Card card = trav.next.card;
        tail = trav;
        tail.next = null;
        length--;
        return card;
    }
    
    public LinkedCardList remove(int index) { // 0-index based
        if (index < 0 || head == null) {
            System.out.println("There is nothing to remove!");
            return null;
        }
        if (index > length - 1) {
            System.out.println("Out of bounds!!");
            return null;
        }
        LinkedCardList group;
        if (index == 0) {
            group = new LinkedCardList(head);
            head = null;
            tail = null;
            length = 0;
            return group;
        }
        Node trav = head;
        int count = 0;
        while (count < index - 1) {
            trav = trav.next;
            count++;
        }
        group = new LinkedCardList(trav.next);
        tail = trav;
        tail.next = null;
        length = count + 1;
        return group;
    }

    public String toString() { // returns a String representation of the LinkedList
        if (head == null) {
            return " ";
        }
        StringBuilder list = new StringBuilder(head.card.toString());

        Node trav = head.next;
        while (trav != null) {  
            list.append(", ");
            list.append(trav.card.toString());
            trav = trav.next;
        }
        return list.toString();
    }

}
