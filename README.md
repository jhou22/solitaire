# solitaire

This is a remake of Solitaire, written in Java

Features:

- Keeps track of how many tries you take to finish the game
- Savestates, being able to redo and undo Savestates
- Console format
- A nice message that congratulates you for finishing the game!
