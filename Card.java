package solitaire;

/*
 * any card in a 52-deck of cards (does NOT include the joker card)
 */
public class Card {
    private String suit;
    private int rank;
    private boolean state; // If the card is placed with the front facing towards you, the state is true,
                           // else it's false

    public Card(int rank, String suit, boolean state) {
        this.rank = rank;
        this.suit = suit;
        this.state = state;
    }

    public Card(int rank, String suit) {
        this(rank, suit, true);
    }

    // get methods
    public String toString() {
        if (state == true) {
            if (rank == 11) {
                return "Jack of " + suit;
            } else if (rank == 12) {
                return "Queen of " + suit;
            } else if (rank == 13) {
                return "King of " + suit;
            } else {
                return rank + " of " + suit;
            }
        } else {
            return "N/A";
        }

    }

    public String getSuit() {
        if (state) {
            return suit;
        } else {
            return "N/A";
        }

    }

    public int getRankNum() { // for comparison of ranks because comparing strings is more difficult than
                              // numbers
        if (state) {
            return rank;
        } else {
            return -1;
        }
    }

    public String getRank() { // returns the ranks based on the actual representation (i.e. 13 is king, 12 is
                              // queen, etc.)
        if (state) {
            if (rank == 11) {
                return "Jack";
            } else if (rank == 12) {
                return "Queen";
            } else if (rank == 13) {
                return "King";
            } else {
                return Integer.toString(rank);
            }
        } else {
            return ("N/A");
        }
    }

    public boolean getState() {
        return state;
    }
    
    // set methods
    public void setState(boolean state) {
        this.state = state;
    }
}
