package solitaire;

import java.util.*;
import java.awt.*;

public class SolitaireClient {
    public static void main(String[] args) {
        ArrayList<SolitaireState> saves = new ArrayList<>(); // saves
        int stateIndex = -1; // what number the save is at (0 based)
        Scanner console = new Scanner(System.in);
        Solitaire game = new Solitaire();

        System.out.println("Welcome to Solitaire!");
        int tries = 0;
        int choice = 0;
        do {
            String value = "";

            System.out.println("Your deck:");
            System.out.println(game.printTableau());
            System.out.println(
                    "Options: \n 1) draw card from hand | 2) move group to cards to another column | 3) move card to foundation | 4) move last card from hand to deck | 5) undo 1 step | 6) redo 1 step | 7) print everything | 8) print tableau | 9) print foundation | 10) print hand | 11) quit");
            value = console.next();
            while (!isANumber(value) || (Integer.parseInt(value) < 1 || Integer.parseInt(value) > 11)) {
                System.out.println("Invalid choice! Try again!");
                System.out.println(
                        "Options: \n 1) draw card from hand | 2) move group to cards to another column | 3) move card to foundation | 4) move last card from hand to deck | 5) undo 1 step | 6) redo 1 step | 7) print everything | 8) print tableau | 9) print foundation | 10) print hand | 11) quit");
                value = console.next();
            }

            choice = Integer.parseInt(value);
            boolean b = false;
            switch (choice) {
                case 1:
                    if (!game.canAddToHand()) {
                        System.out.println("There are no more cards left!");

                    } else {
                        if (stateIndex < saves.size() - 1) {
                            saves.set(stateIndex, game.addToHand());
                        } else {
                            saves.add(game.addToHand());
                        }
                        System.out.println("Your hand is: " + game.getHand().toString());
                        stateIndex++;
                        tries++;
                    }
                    break;
                case 2:
                    String source = "";
                    int destination = 0;
                    while (!b) {
                        System.out.println("Type \"q\" to stop at any time");

                        System.out.println("Type in the card that you want to start moving (casing doesn't matter): ");
                        console.nextLine();
                        value = console.nextLine().toLowerCase();

                        if (value.equals("q")) {
                            break;
                        }
                        while (!game.containKey(value)) {
                            console.nextLine();
                            System.out.println("Invalid card! Try again!");
                            System.out.println(
                                    "Type in the card that you want to start moving (casing doesn't matter): ");
                            value = console.nextLine().toLowerCase();
                        }
                        source = value;
                        System.out.println("Type in the column number where you want to move this group of cards: ");
                        value = console.next(); // in the event that the player types numbers separated by whitespace

                        if (value.equals("q")) {
                            break;
                        }

                        while (!isANumber(value) || (Integer.parseInt(value) < 1 || Integer.parseInt(value) > 7)) {
                            System.out.println("Not a valid number! Try again!");
                            System.out
                                    .println("Type in the column number where you want to move this group of cards: ");
                            value = console.next();
                        }
                        destination = Integer.parseInt(value) - 1;

                        if (game.canMoveTableau(source, destination)) {
                            b = true;
                            if (stateIndex < saves.size() - 1) {
                                saves.set(stateIndex, game.moveTableau(source, destination));
                            } else {
                                saves.add(game.moveTableau(source, destination));
                            }
                            stateIndex++;
                            tries++;
                        } else {
                            System.out.println("Invalid move! Try again!");
                        }

                    }
                    b = false;
                    break;
                case 3:
                    int col = 0;
                    System.out.println("Type \"q\" to stop at any time");
                    while (!b) {
                        System.out
                                .println(
                                        "Type the column number where you want to move the last card to the foundation:");

                        value = console.next();
                        if (value.equals("q")) {
                            break;
                        }
                        while (!isANumber(value) || (Integer.parseInt(value) < 1 || Integer.parseInt(value) > 4)) {
                            System.out.println(
                                    "Not a valid location! Try again! (Reminder that each foundation deck contains only cards of a certain suit)");
                            System.out.println(
                                    "Type the column number where you want to move the last card to the foundation:");
                            value = console.next();
                        }
                        col = Integer.parseInt(value) - 1;
                        if (game.canMoveFoundation(col)) {
                            b = true;
                            if (stateIndex < saves.size() - 1) {
                                saves.set(stateIndex, game.moveFoundation(col));
                            } else {
                                saves.add(game.moveFoundation(col));
                            }
                            stateIndex++;
                            tries++;
                        } else {
                            System.out.println("Invalid move! Try again!");
                        }

                    }
                    b = false;
                    break;
                case 4:
                    int column = 0;
                    while (!b) {
                        System.out.println("Type \"q\" to stop at any time");
                        System.out.println("Type the column number where you want to move the card to:");

                        value = console.next();

                        if (value.equals("q")) {
                            break;
                        }

                        while (!isANumber(value) || (Integer.parseInt(value) < 1 || Integer.parseInt(value) > 7)) {
                            System.out.println(
                                    "Not a valid location! Try again!");
                            System.out.println(
                                    "Type the column number where you want to move the card to:");
                            value = console.next();
                        }
                        column = Integer.parseInt(value) - 1;
                        if (game.canMoveHand(column)) {
                            b = true;
                            if (stateIndex < saves.size() - 1) {
                                saves.set(stateIndex, game.moveHand(column));
                            } else {
                                saves.add(game.moveHand(column));
                            }
                            stateIndex++;
                            tries++;
                        } else {
                            System.out.println("Invalid move! Try again!");
                        }
                    }
                    b = false;
                    break;
                case 5:
                    if (stateIndex - 1 > 0) {
                        game.restore(saves.get(stateIndex - 1));
                        tries++;
                    } else {
                        System.out.println("Can't restore!");
                    }
                    break;

                case 6:
                    if (stateIndex + 1 < saves.size()) {
                        game.restore(saves.get(stateIndex + 1));
                        tries--;
                    } else {
                        System.out.println("Can't restore!");
                    }
                    break;

                case 7:
                    System.out.println("tableau:");
                    System.out.println(game.printTableau());
                    System.out.println("foundation:");
                    System.out.println(game.printFoundation());
                    System.out.println("hand:");
                    System.out.println(game.printHand());
                    break;

                case 8:
                    System.out.println("tableau:");
                    System.out.println(game.printTableau());
                    break;

                case 9:
                    System.out.println("foundation:");
                    System.out.println(game.printFoundation());
                    break;

                case 10:
                    System.out.println("hand:");
                    System.out.println(game.printHand());
                    break;

                case 11:
                    break;
                default:
                    choice = 11;
                    break;
            }
            
        } while (!game.winCondition() && choice != 11);

        System.out.println("You beat the game in " + tries + "tries!");
        console.close();
    }

    public static boolean isANumber(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
